<?php
session_start();
require_once('includes/config.php');
if(isset($_GET["logout"])) {
  unset($_SESSION['access_token']);
  unset($_SESSION['name']);
  unset($_SESSION['id']);
  unset($_SESSION['profile_picture_url']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
<br><br><br>
<div class = "col-xs-3 visible-md visible-lg hidden-sm hidden-xs">

</div>
<div class = "col-xs-6 visible-md visible-lg hidden-sm hidden-xs">
<div class="container-fluid visible-md visible-lg hidden-sm hidden-xs">    
  <center>
  <br><br>
	<div class = "jumbotron" style="background-color:rgba(255, 255, 255, 0.6);color:#800000;">
	<h1><b>Guest House Online</b></h1>
	<br><br>
	
		<a href = "browse.php" class="btn btn-primary btn-lg">I want to book guesthouse</a><br><br>
		<a href = "host_guest_house.php" class="btn btn-success btn-lg">I want to host my guesthouse</a>
	</div></center>
	
	<!--cards-->
   <hr>
   <div class = "row">
     <div class = "col-xs-8">
    <h3 style = "color:maroon;"><b>Search for guesthouses in your area</b></h3>
   
    <p style = "float:left;">Our search engine provides you options to search either by area, price or by availability on a particular date!</p>
    <a href="browse.php" class="btn btn-primary">Try it out</a>
    </div>
    
    <div class = "col-xs-4">
   
    <img src = "images/gho_searchengine.png" style = "float:right;height:150px;width:auto;"></img>
 	</div>
 	</div>
 <br><br>
  <hr>
  <div class = "row">
  <div class = "col-xs-8">
    <h3 style = "color:maroon;"><b>Book online</b></h3>
  <p style = "float:left;">You can book guesthouses online. The owners will then contact you</p>
    <a href="browse_by_area.php" class="btn btn-primary">Choose a guesthouse to book</a>
    
    </div>
    <div class = "col-xs-4">
   <img src = "images/gho_booking.png" style = "float:right;height:150px;width:auto;"></img>
    </div>
    </div>
 <hr><br>
<div class="row">
<div class = "col-xs-8">
<h3 style = "color:maroon;"><b>Check our facebook page</b></h3><br>
<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fguesthouseonline&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=565720713638680" width="340" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
</div>
<div class = "col-xs-4">
<h3 style = "color:maroon;"><b>Check our Instagram</b></h3><br>
<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:59.624413145539904% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BPNoqb1AQJr/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#mummailoveuthemost😘😘</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by guesthouseonline.co.in (@guesthouseonline.co.in) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-01-13T17:32:36+00:00">Jan 13, 2017 at 9:32am PST</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
</div>
</div>
<hr><br><br>
</div>
</div>
<div class = "col-xs-3 visible-md visible-lg hidden-sm hidden-xs">
</div>
<div class="col-xs-12 hidden-md hidden-lg visible-sm visible-xs">
  <br><br>
	<center>
	<div class = "jumbotron" style="background-color:rgba(255, 255, 255, 0.6);color:#800000;">
	<h1><b>Guest House Online</b></h1>
	<br><br>
	
		<a href = "browse.php" class="btn btn-primary btn-md">I want to book guesthouse</a><br><br>
		<a href = "host_guest_house.php" class="btn btn-success btn-md">I want to host my guesthouse</a>
	</div>
	
	</center>
	<hr>
	<div class = "row">
	<div class = "col-xs-8"> 
    <h3 style = "color:maroon;"><b>Search for guesthouses in your area</b></h3>
   
    <p style = "float:left;">Our search engine provides you options to search either by area, price or by availability on a particular date!</p>
    <a href="browse.php" class="btn btn-primary">Try it out</a>
    </div>
    <div class = "col-xs-4">
   
 	</div>
 	</div>
	 <br><br>
  <hr>
  <div class="row">
  <div class = "col-xs-8">
    <h3 style = "color:maroon;"><b>Book online</b></h3>
  <p style = "float:left;">You can book guesthouses online. The owners will then contact you</p>
    <a href="browse_by_area.php" class="btn btn-primary">Choose a guesthouse to book</a>
    
    </div>
</div>
	<hr>

<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fguesthouseonline&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=565720713638680" width="250" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
<br><hr>
<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:59.624413145539904% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BPNoqb1AQJr/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#mummailoveuthemost😘😘</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by guesthouseonline.co.in (@guesthouseonline.co.in) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-01-13T17:32:36+00:00">Jan 13, 2017 at 9:32am PST</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
<br><br><br>
</div>
<!--<footer style = "background-color:white;min-height:100px;box-shadow: 1px 4px 4px 5px rgba(0, 0, 0, .2);color:black;">
<center>
  <p>The content presented in guesthouseonline is put up by the owners of the guest houses or the caterers or transportation services.</p>
  <p>Their full consent is involved in presenting this material and showing it to visitors.</p>
  <hr>
  <p>made with &hearts; by <a href = "http://keyboardsan.org" target = "_blank" >keyboardsan.org</a></p>
 </center>
</footer>-->

</body>
</html>
