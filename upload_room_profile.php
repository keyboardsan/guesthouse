<?php
session_start();
require_once('includes/config.php');
unset($_SESSION['errorMessage']);
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$valid_session = 0;
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  	}
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: http://guesthouseonline.co.in');
  	exit();
}

$j = 0;
$default_pictures = array();
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT name FROM pictures where google_id = '".$_SESSION['id']."' and registrationNumber = '".$_GET['registrationNumber']."' and id = '".$_GET['room_number']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
           $default_pictures[$j] = $row["name"];
           $j++;
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$conn = null;
$target_dir = "uploads/";
//Check if image is empty
$i = 0;
$gallery_array = array();
for($i = 0; $i<10; $i++) {
if(strlen(basename($_FILES["fileToUpload_".(string)$i]["name"]))<=0) {
} else {
$target_file = $target_dir.$_SESSION['id'].$_POST['registration_number']. basename($_FILES["fileToUpload_".(string)$i]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload_".(string)$i]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
        $_SESSION['slideshowerrorMessage'] = "File ".(string)($i+1)." is not an image";
        $conn = null;
        header('Location: edit_room_profile.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET['room_number'].'');
        exit();
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    $uploadOk = 0;
    $_SESSION['slideshowErrorMessage'] = "File ".(string)($i+1)." already exists";
    $conn = null;
    header('Location: edit_room_profile.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET['room_number'].'');
    exit();
}
// Check file size
if ($_FILES["fileToUpload_".(string)$i]["size"] > 1500000) {
    $_SESSION['slideshowErrorMessage'] = "File ".(string)($i+1)." is too large. Image size must not be more than 1.4 MB";
    $uploadOk = 0;
    $conn = null;
    header('Location: edit_room_profile.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET['room_number'].'');
    exit();
    
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $_SESSION['slideshowErrorMessage'] = "File ".(string)($i+1)." is not an image";
    $uploadOk = 0;
    $conn = null;
    header('Location: edit_room_profile.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET['room_number'].'');
    exit();
}
// Check if $uploadOk is set to 0 by an error
$temp = explode(".", $_FILES["fileToUpload_".(string)$i]["name"]);
$gallery_array[$i] = round(microtime(true)).$i.'.' . end($temp);
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if(strlen($default_pictures[$i])<=0) {
       if (move_uploaded_file($_FILES["fileToUpload_".(string)$i]["tmp_name"], $target_dir.$gallery_array[$i])) {
    	$conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	$sql = $conn->prepare("INSERT INTO pictures (google_id,registrationNumber,id,name) VALUES (?,?,?,?)");
	 $sql->execute(array($_SESSION["id"],$_GET['registrationNumber'],$_GET['room_number'], $gallery_array[$i]));
	 $conn = null;
	 } else {
        $_SESSION['slideshowErrorMessage'] = "Sorry, there was an error uploading your file ".(string)($i+1)." ";
        $conn = null;
        header('Location: edit_room_profile.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET['room_number'].'');
        exit();
      }
       } else {
	 	move_uploaded_file($_FILES["fileToUpload_".(string)$i]["tmp_name"], $target_dir.$gallery_array[$i]);
	 	$conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 	$sql = "UPDATE pictures SET name = '".$gallery_array[$i]."' WHERE google_id = '".$_SESSION["id"]."' AND registrationNumber = '".$_GET["registrationNumber"]."' and id = '".$_GET["room_number"]."' and name = '".$default_pictures[$i]."'";
 	$statement = $conn->prepare($sql);
 	$statement->execute();
 	unlink("uploads/".$default_pictures[$i]);
	$conn = null;
	 }
    
    }
  }
}
$conn = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
<div class="col-xs-12 text-center">    
<br><br><br>
<?php
try {
    $conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE rooms SET description = '".$_POST["description"]."' WHERE google_id = '".$_SESSION["id"]."' AND registrationNumber = '".$_GET["registrationNumber"]."' AND room_number = '".$_GET["room_number"]."'";

    // Prepare statement
    $stmt = $conn->prepare($sql);

    // execute the query
    $stmt->execute();
    echo '<br><h4>Your room profile has been updated</h4><br><h4>Visit your room page to see the changes</h4>';
    }
catch(PDOException $e)
    {
    echo '<br>'.$e->getMessage();
    }
$conn = null;
?>
</div>
</body>
</html>