<?php
session_start();
require_once('includes/config.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="moment-with-locales.js"></script>
  <script src="bootstrap-datetimepicker.js"></script>
  <link href="bootstrap-datetimepicker.css" rel="stylesheet">
  <link rel = "stylesheet" href = "style/my_style.css">
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker();
            });
        </script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
 
<div class="col-xs-6 visible-md visible-lg hidden-sm hidden-xs">
    
  <br><br><br>
  <br><p class = "bg-danger"><?php echo $_SESSION["bookingErrorMessage"]; ?></p><br>
  <h3>Search for guest houses</h3><br>
  <form method = "post" action = "search_guest_house.php">
  <h4>Select area</h4>
<select class="form-control" id="area" name = "area" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['area']; } ?>" required>
    <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT * FROM area";

    if ($result=mysqli_query($mysqli,$sql))
  {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<option>'.$row["name"].'</option>';
    }
  // Free result set
  mysqli_free_result($result);
}

mysqli_close($mysqli);
?>
</select>
<br>
<input type = "submit" name = "submit" value = "Go" class = "btn btn-success">
</form>
<br>
<h4>Or</h4>
<br>
<h4>Enter a time slot</h4>
  <form method = "post" action = "search_by_time.php">
   	   
   	    	
   	   
            From<input type='text' name = "time_from" class="form-control" id='datetimepicker4' />
            
           
            To<input type='text' name = "time_to" class="form-control" id='datetimepicker5' />
            <br><br>
           
            <input type = "submit" name = "submit" value = "submit" class = "btn btn-success">
           
  </form>
<br>
<h4>Or</h4><br>
<a href = "browse_by_area.php" class = "btn btn-primary">Browse all guest houses</a>
</div>
<div class="col-xs-6 visible-md visible-lg hidden-sm hidden-xs">
<br><br><br>
<script type="text/javascript" language="javascript">
      var aax_size='300x600';
      var aax_pubname = 'keyboardsan-21';
      var aax_src='302';
    </script>
    <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
	
<script type="text/javascript" language="javascript">
      var aax_size='160x600';
      var aax_pubname = 'keyboardsan-21';
      var aax_src='302';
    </script>
    <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>

</div>

<div class="col-xs-12 hidden-md hidden-lg visible-sm visible-xs">
    
  <br><br><br>
  <br><p class = "bg-danger"><?php echo $_SESSION["bookingErrorMessage"]; ?></p><br>
  <h3>Search for guest houses</h3><br>
  <form method = "post" action = "search_guest_house.php">
  <h4>Select area</h4>
<select class="form-control" id="area" name = "area" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['area']; } ?>" required>
    <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT * FROM area";

    if ($result=mysqli_query($mysqli,$sql))
  {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<option>'.$row["name"].'</option>';
    }
  // Free result set
  mysqli_free_result($result);
}

mysqli_close($mysqli);
?>
</select>
<br>
<input type = "submit" name = "submit" value = "Go" class = "btn btn-success">
</form>
<br>
<h4>Or</h4>
<br>
<h4>Enter a time slot</h4>
  <form method = "post" action = "search_by_time.php">
   	   
   	    	
   	   
            From<input type='text' name = "time_from" class="form-control" id='datetimepicker4' />
            
           
            To<input type='text' name = "time_to" class="form-control" id='datetimepicker5' />
            <br><br>
           
            <input type = "submit" name = "submit" value = "submit" class = "btn btn-success">
           
  </form>
<br>
<h4>Or</h4><br>
<a href = "browse_by_area.php" class = "btn btn-primary">Browse all guest houses</a>
<br><br><br>
<script type="text/javascript" language="javascript">
      var aax_size='300x250';
      var aax_pubname = 'keyboardsan-21';
      var aax_src='302';
    </script>
    <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</div>

</body>
</html>