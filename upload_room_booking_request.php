<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}
date_default_timezone_set('Asia/Kolkata');

$target_dir = "uploads/";
unset($_SESSION["slideshowErrorMessage"]);
unset($_SESSION["errorMessage"]);
  $time_from = new DateTime($_POST['time_from']);
  $time_to = new DateTime($_POST['time_to']);
   $difference = $time_from->diff($time_to);
  if($difference->format('%a%h')<1) {
  	$_SESSION["errorMessage"] = "You have to book a place for atleast 1 hour";
  	header('Location: book_room.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'');
  			exit();
  }
  $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT time_from,time_to FROM status WHERE registrationNumber = '".$_GET['registrationNumber']."' AND place_number = '".$_GET['room_number']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  		$time_from_status = new DateTime($row["time_from"]);
  		$time_to_status = new DateTime($row["time_to"]);
  		if($time_from == $time_from_status) {
  			$_SESSION['errorMessage'] = "This slot is already booked";
  			mysqli_free_result($result);
  			mysqli_close($mysqli);
  			header('Location: book_room.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'');
  			exit();
  		}
  		if($time_from == $time_to_status) {
  			$_SESSION['errorMessage'] = "This slot is already booked";
  			mysqli_free_result($result);
  			mysqli_close($mysqli);
  			header('Location: book_room.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'');
  			exit();
  		}
  		if($time_to > $time_from_status && $time_to < $time_to_status) {
  			$_SESSION['errorMessage'] = "This slot is already booked";
  			mysqli_free_result($result);
  			mysqli_close($mysqli);
  			header('Location: book_room.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'');
  			exit();
  		}
  		if($time_from > $time_from_status && $time_from < $time_to_status) {
  			$_SESSION['errorMessage'] = "This slot is already booked";
  			mysqli_free_result($result);
  			mysqli_close($mysqli);
  			header('Location: book_room.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'');
  			exit();
  		}
  		
  	}
  mysqli_free_result($result);
  }
  $sql="SELECT price_per_hour FROM rooms WHERE registrationNumber = '".$_GET['registrationNumber']."' AND room_number = '".$_GET['room_number']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$price_per_hour = $row["price_per_hour"];
  	}
  
  mysqli_free_result($result);
  }
$interval = $time_from->diff($time_to);
$amount = floatval(($interval->format('%a%i'))*60)*floatval(($price_per_hour/60));
mysqli_close($mysqli);

try {
    $conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = $conn->prepare("INSERT INTO status (customer_id, registrationNumber, place_number,time_from, time_to,booking_time, amount, confirmation, completion) VALUES (?,?,?,?,?,?,?,?,?)");
    $booking_time = date("Ymdhisa");
	 $sql->execute(array($_SESSION['id'],$_GET["registrationNumber"],$_GET["room_number"],$_POST["time_from"],$_POST["time_to"],$booking_time, $amount,"pending","pending"));
    }
catch(PDOException $e)
    {
    echo '<br>'.$e->getMessage();
    }
$conn = null;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <script src="moment-with-locales.js"></script>
  <script src="bootstrap-datetimepicker.js"></script>
  <link href="bootstrap-datetimepicker.css" rel="stylesheet">
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker();
            });
        </script>
  
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center" style = "width:300px;">    
  <div class="row">
<br><br><br>
<?php
require_once 'classes/phpmailer/mail.php';
ini_set("include_path", '/home/nishantshekhar95/php:' . ini_get("include_path") );
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT google_id FROM guesthouse WHERE registrationNumber = '".$_GET['registrationNumber']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$owner_id = $row["google_id"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT google_email FROM google_users WHERE google_id = '".$owner_id."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$owner_email = $row["google_email"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT google_email FROM google_users WHERE google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$customer_email = $row["google_email"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber,place_number,time_from,time_to,booking_time,amount,amount_paid,confirmation,completion FROM status WHERE booking_time = '".$booking_time."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$registrationNumber = $row["registrationNumber"];
  	$room_number = $row["place_number"];
  	$time_from = $row["time_from"];
  	$time_to = $row["time_to"];
  	$booking_time = $row["booking_time"];
  	$amount = $row["amount"];
  	$amount_paid = $row["amount_paid"];
  	$confirmation = $row["confirmation"];
  	$completion = $row["completion"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mail1 = new PHPMailer(); // create a new object
$mail1->IsSMTP(); // enable SMTP
$mail1->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail1->SMTPAuth = true; // authentication enabled
 // secure transfer enabled REQUIRED for Gmail
$mail1->Host = "mail.guesthouseonline.co.in";
$mail1->Port = 25; // or 587
$mail1->IsHTML(true);
$mail1->Username = "no-reply@guesthouseonline.co.in";
$mail1->Password = "Fuckincool@69";
$mail1->SetFrom("no-reply@guesthouseonline.co.in");
$mail1->Subject = "Guest House Online: Booking request received";
$mail1->Body = "<p>A customer has booked your room number: ".$_GET["room_number"]."</p><p>Belonging to guest house registration number: ".$_GET["registrationNumber"]."</p><p>Visit the following link to manage this booking</p><p>manage_room.php?registrationNumber=".$_GET["registrationNumber"]."&room_number=".$_GET["room_number"];
$mail1->AddAddress($owner_email);
$mail1->SmtpClose();

 if(!$mail1->Send()) {
     
 } else {
    
 }
$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
 // secure transfer enabled REQUIRED for Gmail
$mail->Host = "mail.guesthouseonline.co.in";
$mail->Port = 25; // or 587
$mail->IsHTML(true);
$mail->Username = "no-reply@guesthouseonline.co.in";
$mail->Password = "Fuckincool@69";
$mail->SetFrom("no-reply@guesthouseonline.co.in");
$mail->Subject = "Guest House Online: Booking request made";
$mail->Body = "<p>You have booked a guest house with registration number: ".$registrationNumber." and room number: ".$room_number."</p><p>The current state of your request is as follows:</p>
<p>registrationNumber = ".$registrationNumber."</p>
<p>room_number = ".$room_number."</p>
<p>time_from = ".$time_from."</p> 
<p>time_to = ".$time_to."</p>
<p>booking_time = ".$booking_time."</p>
<p>amount = ".$amount."</p> 
<p>amount_paid = ".$amount_paid."</p>
<p>confirmation = ".$confirmation."</p>
<p>completion = ".$completion."</p>
<p>Visit the following link to manage your bookings</p><p>manage_booking.php";
$mail->AddAddress($customer_email);

 if(!$mail->Send()) {
    
 } else {
    
 }
 $mail->SmtpClose();
?>
<script>
window.location.replace("booking_confirmation.php");
</script>
<h4>Your booking request has been recorded</h4>
<h4>We will notify you when your request will be accepted</h4>
<h4>Visit your dashboard to manage or cancel your request</h4>
</div>
</div>
</body>
</html>