<?php
session_start();
require_once('includes/config.php');
unset($_SESSION['errorMessage']);
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
try {
    $conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $sql = "UPDATE google_users SET country_code = ".(intval($_POST["country_code"])).", phone_number = ".(intval($_POST["phone_number"]))." WHERE google_id = '".$_SESSION["id"]."'";

    // Prepare statement
    $stmt = $conn->prepare($sql);

    // execute the query
    $stmt->execute();
    header('Location: dashboard.php');
}
catch(PDOException $e)
    {
    echo '<br>'.$e->getMessage();
    }
$conn = null;

?>