<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
$valid_session = 0;
unset($_SESSION["slideshowErrorMessage"]);
unset($_SESSION["errorMessage"]);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  	}
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: http://guesthouseonline.co.in');
  	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="container-fluid text-center visible-md visible-lg hidden-sm hidden-xs">    
  <br><br><br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
<?php
    $i = 0;
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT name FROM pictures where google_id = '".$_SESSION['id']."' and registrationNumber = '".$_GET["registrationNumber"]."' and id = '".$_GET["room_number"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  if(mysqli_num_rows($result)<=0) {
  	 echo ' <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>
   <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="images/image_not_found.jpg" class="img-responsive">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" class="img-responsive">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" class="img-responsive">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" class="img-responsive">
    </div>
  </div>';
  } else {
  while ($row=mysqli_fetch_assoc($result))
    {
   if($i>0) {
    echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
    $i++;
  } else {
  echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
  $i++;
  }       
  }
  $i = 0;
  echo '</ol><div class="carousel-inner" role="listbox">';
  $result=mysqli_query($mysqli,$sql);
  while ($row=mysqli_fetch_assoc($result)) {
  	if($i>0) {
  	echo ' <div class="item">
      <img src="'.$target_dir.$row["name"].'" style = "height:400px;width:auto;" >
    </div>';
    $i++;
    }else {
    echo '<div class="item active">
      <img src="'.$target_dir.$row["name"].'" style = "height:400px;width:auto;" >
    </div>';
    $i++;
  }
 }
 echo '</div>';
}
 mysqli_free_result($result);
}
mysqli_close($mysqli);
if(isset($_SESSION["registrationNumber"])) {
$registrationNumber = $_SESSION["registrationNumber"];
}else {
$registrationNumber = $_GET["registrationNumber"];
}
?>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<a href = "edit_room_profile.php?registrationNumber=<?php echo $registrationNumber;?>&room_number=<?php echo $_GET['room_number']; ?>" target = "_blank" ><img src = "images/edit_icon.png" width = "15" height = "auto" title = "edit room profile"></img><h4>Edit your room profile</h4></a><hr>
<?php
	$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT thumbnail,guestHouseName,area,description FROM guesthouse where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$guestHouseName = $row["guestHouseName"];
  	$thumbnail = $row["thumbnail"];
  	$area = $row["area"];
  	}
  mysqli_free_result($result);
  }
  
   $sql="SELECT room_number,description FROM rooms where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."' and room_number = '".$_GET["room_number"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$room_number = $row["room_number"];
  	$description = $row["description"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
?>
<h3><?php echo $guestHouseName; ?>, <?php echo $area; ?></h3>
<h3>Room Number: <?php echo $room_number; ?></h3><br>
<h4><?php echo nl2br($description); ?></h4><br>
<h4>Booking Status</h4><br><br>


<div class = "table-responsive">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Time from</th>
        <th>Time to</th>
        <th>Amount</th>
        <th>Amount Paid</th>
        <th>Confirmation</th>
        <th>Completion</th>
        <th>Manage</th>
      </tr>
    </thead>
    <tbody>
<?php
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT time_from,time_to,booking_time, amount,amount_paid,confirmation,completion,google_users.google_name FROM status,google_users where registrationNumber = '".$registrationNumber."' and place_number = '".$_GET["room_number"]."' and status.customer_id = google_users.google_id ORDER BY booking_time DESC";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	    echo '
  	        <tr>
  	  	<td>'.$row["google_name"].'</td>
  	  	<td>'.$row["time_from"].'</td>
  	  	<td>'.$row["time_to"].'</td>
  	  	<td>'.$row["amount"].'</td>
  	  	<td>'.$row["amount_paid"].'</td>
  	  	<td>'.$row["confirmation"].'</td>
  	  	<td>'.$row["completion"].'</td>
  	  	<td><a href = "manage_room_booking.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'&booking_time='.$row["booking_time"].'" class = "btn btn-default">Manage</a></td></tr>
  	  	
  	';
  	}
  mysqli_free_result($result);
  }
	

mysqli_close($mysqli);
?>
</table>
</div>
</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs">    
  <br><br><br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
<?php
    $i = 0;
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT name FROM pictures where google_id = '".$_SESSION['id']."' and registrationNumber = '".$_GET["registrationNumber"]."' and id = '".$_GET["room_number"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  if(mysqli_num_rows($result)<=0) {
  	 echo ' <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>
   <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="images/image_not_found.jpg" style = "height:400px;width:auto;">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" style = "height:400px;width:auto;">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" style = "height:400px;width:auto;">
    </div>

    <div class="item">
      <img src="images/image_not_found.jpg" style = "height:400px;width:auto;">
    </div>
  </div>';
  } else {
  while ($row=mysqli_fetch_assoc($result))
    {
   if($i>0) {
    echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
    $i++;
  } else {
  echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
  $i++;
  }       
  }
  $i = 0;
  echo '</ol><div class="carousel-inner" role="listbox">';
  $result=mysqli_query($mysqli,$sql);
  while ($row=mysqli_fetch_assoc($result)) {
  	if($i>0) {
  	echo ' <div class="item">
      <img src="'.$target_dir.$row["name"].'" style = "height:400px;width:auto;" >
    </div>';
    $i++;
    }else {
    echo '<div class="item active">
      <img src="'.$target_dir.$row["name"].'" style = "height:400px;width:auto;" >
    </div>';
    $i++;
  }
 }
 echo '</div>';
}
 mysqli_free_result($result);
}
mysqli_close($mysqli);
if(isset($_SESSION["registrationNumber"])) {
$registrationNumber = $_SESSION["registrationNumber"];
}else {
$registrationNumber = $_GET["registrationNumber"];
}
?>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<a href = "edit_room_profile.php?registrationNumber=<?php echo $registrationNumber;?>&room_number=<?php echo $_GET['room_number']; ?>" target = "_blank" ><img src = "images/edit_icon.png" width = "15" height = "auto" title = "edit room profile"></img><h4>Edit your room profile</h4></a><hr>
<?php
	$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT thumbnail,guestHouseName,area,description FROM guesthouse where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$guestHouseName = $row["guestHouseName"];
  	$thumbnail = $row["thumbnail"];
  	$area = $row["area"];
  	}
  mysqli_free_result($result);
  }
  
   $sql="SELECT room_number,description FROM rooms where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."' and room_number = '".$_GET["room_number"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$room_number = $row["room_number"];
  	$description = $row["description"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
?>
<h3><?php echo $guestHouseName; ?>, <?php echo $area; ?></h3>
<h3>Room Number: <?php echo $room_number; ?></h3><br>
<h4><?php echo nl2br($description); ?></h4><br>
<h4>Booking Status</h4><br><br>


<div class = "table-responsive">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Time from</th>
        <th>Time to</th>
        <th>Amount</th>
        <th>Amount Paid</th>
        <th>Confirmation</th>
        <th>Completion</th>
        <th>Manage</th>
      </tr>
    </thead>
    <tbody>
<?php
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT time_from,time_to,booking_time, amount,amount_paid,confirmation,completion,google_users.google_name FROM status,google_users where registrationNumber = '".$registrationNumber."' and place_number = '".$_GET["room_number"]."' and status.customer_id = google_users.google_id ORDER BY booking_time DESC";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	    echo '
  	        <tr>
  	  	<td>'.$row["google_name"].'</td>
  	  	<td>'.$row["time_from"].'</td>
  	  	<td>'.$row["time_to"].'</td>
  	  	<td>'.$row["amount"].'</td>
  	  	<td>'.$row["amount_paid"].'</td>
  	  	<td>'.$row["confirmation"].'</td>
  	  	<td>'.$row["completion"].'</td>
  	  	<td><a href = "manage_room_booking.php?registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'&booking_time='.$row["booking_time"].'" class = "btn btn-default">Manage</a></td></tr>
  	  	
  	';
  	}
  mysqli_free_result($result);
  }
	

mysqli_close($mysqli);
?>
</table>
</div>
</div>
</body>
</html>