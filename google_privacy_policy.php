<?php 
 session_start();
 require_once('includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="container-fluid text-center visible-md visible-lg hidden-sm hidden-xs">    
  <center>
  <br><br><br>
  <h1>Guest House Online</h1><br>
  <h4>Privacy Policy</h4>
  <p>We are going to save the following from your Google account to our database:</p>
  <p>Your google ID</p>
  <p>Your email address</p>
  <p>Your Name</p>
  <p>Your Google Plus profile url</p>
  <p>Your profile picture url</p>
  <br><br>
  <hr>
  <h4>We declare that we are not going to share your data with any third party whatsoever</h4><br>
  <h4>Thank you</h4>
</center>
</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs">    
  <center>
  <br><br><br>
  <h1>Guest House Online</h1><br>
  <h4>Privacy Policy</h4>
  <p>We are going to save the following from your Google account to our database:</p>
  <p>Your google ID</p>
  <p>Your email address</p>
  <p>Your Name</p>
  <p>Your Google Plus profile url</p>
  <p>Your profile picture url</p>
  <br><br>
  <hr>
  <h4>We declare that we are not going to share your data with any third party whatsoever</h4><br>
  <h4>Thank you</h4>
</center>
</div>
</body>
</html>