<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
<div class="col-xs-6 text-center visible-md visible-lg hidden-sm hidden-xs" >    
  
  <br><br><br>
  <center>
<form action = "upload_guest_house_data.php" method = "post" enctype="multipart/form-data" style = "width:300px;">  
<h4>&emsp; &emsp; Enter registration number &emsp; <a href = "registration_number_info.php"><img src = "images/question_mark.png" width = "15" height = "auto" title = "Registration number is a unique number which is provided to your guest house at the time of registration. The UP government provides a registration number after the business has been approved. This number will be verified by us."></img></a></h4><br>
  <input type = "text" name = "registration_number" id = "registration_number" placeholder = "registration number" class = "form-control input-sm" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['registration_number']; } ?>"></input><br>
  <input type = "checkbox" name = "registered" id = "registered" value = "false"> I do not have a registration number<br><br> 
  <h4>Guest house name</h4>
  <input type = "text" name = "name" id = "name" placeholder = "guest house name" class = "form-control input-sm" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['name']; } ?>" required></input><br>
  <h4>Upload a thumbnail</h4>
  
  <label class="btn btn-primary" for="my-file-selector">
    <input id="my-file-selector"  type="file" name = "fileToUpload" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
    Browse
</label>
<span class='label label-info' id="upload-file-info"></span>
<p class="bg-danger"><?php echo $_SESSION['errorMessage']; ?></p><br><br>
<h4>Select area</h4>
<select class="form-control" id="area" name = "area" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['area']; } ?>" required>
    <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT * FROM area";

    if ($result=mysqli_query($mysqli,$sql))
  {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<option>'.$row["name"].'</option>';
    }
  // Free result set
  mysqli_free_result($result);
}

mysqli_close($mysqli);
?>
  </select><br><br>
<h4>Enter full address</h4>
<textarea class="form-control" rows="5" id="address" name = "address" placeholder = "full address of the guest house" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['address']; } ?>" required></textarea><br>
<input type = "submit" class = "btn btn-success" value = "submit"></input><br>
</form>
</center>
<br><br>
</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs" >    
  
  <br><br><br>
  <center>
<form action = "upload_guest_house_data.php" method = "post" enctype="multipart/form-data" style = "width:300px;">  
<h4>&emsp; &emsp; Enter registration number &emsp; <a href = "registration_number_info.php"><img src = "images/question_mark.png" width = "15" height = "auto" title = "Registration number is a unique number which is provided to your guest house at the time of registration. The UP government provides a registration number after the business has been approved. This number will be verified by us."></img></a></h4><br>
  <input type = "text" name = "registration_number" id = "registration_number" placeholder = "registration number" class = "form-control input-sm" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['registration_number']; } ?>"></input><br>
  <input type = "checkbox" name = "registered" id = "registered" value = "false"> I do not have a registration number<br><br> 
  <h4>Guest house name</h4>
  <input type = "text" name = "name" id = "name" placeholder = "guest house name" class = "form-control input-sm" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['name']; } ?>" required></input><br>
  <h4>Upload a thumbnail</h4>
  
  <label class="btn btn-primary" for="my-file-selector">
    <input id="my-file-selector"  type="file" name = "fileToUpload" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
    Browse
</label>
<span class='label label-info' id="upload-file-info"></span>
<p class="bg-danger"><?php echo $_SESSION['errorMessage']; ?></p><br><br>
<h4>Select area</h4>
<select class="form-control" id="area" name = "area" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['area']; } ?>" required>
    <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT * FROM area";

    if ($result=mysqli_query($mysqli,$sql))
  {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<option>'.$row["name"].'</option>';
    }
  // Free result set
  mysqli_free_result($result);
}

mysqli_close($mysqli);
?>
  </select><br><br>
<h4>Enter full address</h4>
<textarea class="form-control" rows="5" id="address" name = "address" placeholder = "full address of the guest house" value="<?php if(isset($_SESSION['errorMessage'])){ echo $_POST['address']; } ?>" required></textarea><br>
<input type = "submit" class = "btn btn-success" value = "submit"></input><br>
</form>
</center>
<br><br>
</div>
</body>
</html>