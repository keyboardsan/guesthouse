<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
$valid_session = 0;
unset($_SESSION["slideshowErrorMessage"]);
unset($_SESSION["errorMessage"]);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  	}
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: http://guesthouseonline.co.in');
  	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="container-fluid text-center">    
  <div class="row">
  <br><br><br>
<?php
  try {
    $conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $sql = "UPDATE status SET amount_paid = ".$_POST["amount_paid"].", confirmation = '".$_POST["confirmation"]."', completion = '".$_POST["completion"]."' WHERE booking_time = '".$_GET["booking_time"]."'";

    // Prepare statement
    $stmt = $conn->prepare($sql);

    // execute the query
    $stmt->execute();
    echo '<h3>Record updated</h3>';
    }
catch(PDOException $e)
    {
    echo '<br>'.$e->getMessage();
    }
$conn = null;
require_once 'classes/phpmailer/mail.php';
ini_set("include_path", '/home/nishantshekhar95/php:' . ini_get("include_path") );
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT customer_id FROM status WHERE booking_time = '".$_GET['booking_time']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$customer_id = $row["customer_id"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT google_email FROM google_users WHERE google_id = '".$customer_id."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$customer_email = $row["google_email"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber,place_number,time_from,time_to,booking_time,amount,amount_paid,confirmation,completion FROM status WHERE booking_time = '".$_GET["booking_time"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$registrationNumber = $row["registrationNumber"];
  	$hall_number = $row["place_number"];
  	$time_from = $row["time_from"];
  	$time_to = $row["time_to"];
  	$booking_time = $row["booking_time"];
  	$amount = $row["amount"];
  	$amount_paid = $row["amount_paid"];
  	$confirmation = $row["confirmation"];
  	$completion = $row["completion"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
 // secure transfer enabled REQUIRED for Gmail
$mail->Host = "mail.guesthouseonline.co.in";
$mail->Port = 25; // or 587
$mail->IsHTML(true);
$mail->Username = "no-reply@guesthouseonline.co.in";
$mail->Password = "Fuckincool@69";
$mail->SetFrom("no-reply@guesthouseonline.co.in");
$mail->Subject = "Guest House Online: Booking request revised";
$mail->Body = "<p>The owner of the guest house registration number: ".$registrationNumber." has received and revised your booking request made on hall number: ".$hall_number."</p><p>The current state of your request is as follows:</p>
<p>registrationNumber = ".$registrationNumber."</p>
<p>hall_number = ".$hall_number."</p>
<p>time_from = ".$time_from."</p> 
<p>time_to = ".$time_to."</p>
<p>booking_time = ".$booking_time."</p>
<p>amount = ".$amount."</p> 
<p>amount_paid = ".$amount_paid."</p>
<p>confirmation = ".$confirmation."</p>
<p>completion = ".$completion."</p>
<p>Visit the following link to manage your bookings</p><p>manage_booking.php
<p>Please note that the owners may delete your request, in which case you will receive this notification but not be able to view the request in your dashboard</p><p>If such a thing happens and you know that there was no reason to delete the request, please let us know.</p><p>Regards,<p>Guest House Online Team</p>";
$mail->AddAddress($customer_email);

 if(!$mail->Send()) {
    
 } else {
    
 }
?>
<script>
window.location.replace("manage_hall_booking.php?booking_time=<?php echo $_GET['booking_time']; ?>&registrationNumber=<?php echo $_GET['registrationNumber']; ?>&hall_number=<?php echo $hall_number; ?>");
</script>
</div>
</div>
</body>
</html>