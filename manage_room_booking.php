<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
$valid_session = 0;
unset($_SESSION["slideshowErrorMessage"]);
unset($_SESSION["errorMessage"]);
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  	}
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: http://guesthouseonline.co.in');
  	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="col-xs-12 text-center">    
  <center>
  <br><br><br>
<?php
if(isset($_SESSION["registrationNumber"])) {
$registrationNumber = $_SESSION["registrationNumber"];
}else {
$registrationNumber = $_GET["registrationNumber"];
}
	$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT thumbnail,guestHouseName,area,description FROM guesthouse where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$guestHouseName = $row["guestHouseName"];
  	$thumbnail = $row["thumbnail"];
  	$area = $row["area"];
  	}
  mysqli_free_result($result);
  }
  
   $sql="SELECT room_number,description FROM rooms where google_id = '".$_SESSION['id']."' and registrationNumber = '".$registrationNumber."' and room_number = '".$_GET["room_number"]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$room_number = $row["room_number"];
  	$description = $row["description"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
?>
<h3><?php echo $guestHouseName; ?>, <?php echo $area; ?></h3>
<h3>Room Number: <?php echo $room_number; ?></h3><br>
<h4>Manage Booking</h4><br><br>


<?php
    
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT customer_id,time_from,time_to,amount,amount_paid,confirmation,completion FROM status where booking_time = '".$_GET["booking_time"]."' ORDER BY booking_time DESC";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$customer_id = $row['customer_id'];
  	}
mysqli_free_result($result);
  }
mysqli_close($mysqli);
 $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
$sql="SELECT google_name,country_code,phone_number,google_email FROM google_users where google_id = '".$customer_id."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	$customer_name = $row['google_name'];
  	$country_code = $row['country_code'];
  	$phone_number = $row['phone_number'];
  	$google_email = $row['google_email'];
  	}
mysqli_free_result($result);
  }
mysqli_close($mysqli);
?>
<div class = "col-xs-12">
<form action = "upload_room_booking.php?registrationNumber=<?php echo $_GET['registrationNumber']; ?>&room_number=<?php echo $_GET['room_number']; ?>&booking_time=<?php echo $_GET['booking_time']; ?>"; method = "post">
<div class = "table-responsive">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Customer Phone No</th>
        <th>Customer Email</th>
        <th>Time from</th>
        <th>Time to</th>
        <th>Amount</th>
        <th>Amount Paid</th>
        <th>Confirmation</th>
        <th>Completion</th>
        <th>Cancel</th>
      </tr>
    </thead>
    <tbody>

<?php
    
    $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT customer_id,time_from,time_to,amount,amount_paid,confirmation,completion FROM status where registrationNumber = '".$registrationNumber."' and place_number = '".$_GET["room_number"]."' and booking_time = '".$_GET["booking_time"]."' ORDER BY booking_time DESC";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	  echo '
  	  	<tr>
  	  	<td>'.$customer_name.'</td>
  	  	<td>+'.$country_code.'&emsp;'.$phone_number.'</td>
  	  	<td>'.$google_email.'</td>
  	  	<td>'.$row["time_from"].'</td>
  	  	<td>'.$row["time_to"].'</td>
  	  	<td>'.$row["amount"].'</td>
  	  	<td><input type = "number" step = "0.00001" name = "amount_paid" value = "'.$row["amount_paid"].'"></td>
  	  	<td> <select name = "confirmation" id = "confirmation" value = "'.$row["confirmation"].'">
  			<option selected>'.$row["confirmation"].'</option>
  			<option value="confirmed">confirmed</option>
  			<option value="pending">pending</option>
		     </select> 
  	  	<td> <select name = "completion" id = "completion" value = "'.$row["completion"].'">
  			<option selected>'.$row["completion"].'</option>
  			<option value="completed">completed</option>
  			<option value="pending">pending</option>
  			<option value="cancelled">cancelled</option>
		     </select>
  	  	<td><a href = "cancel_booking.php?booking_time='.$_GET["booking_time"].'&registrationNumber='.$_GET["registrationNumber"].'&room_number='.$_GET["room_number"].'" class = "btn btn-default" title="Cancelled requests will be permanently deleted">Cancel</a></td>
  	  	
  	';
  	}
  mysqli_free_result($result);
  }
	

mysqli_close($mysqli);
?>
</table>
</div>
<input type = "submit" value = "submit changes" name = "submit" class = "btn btn-success"><br><br>
</form>
</div>
</center>
</div>
</body>
</html>