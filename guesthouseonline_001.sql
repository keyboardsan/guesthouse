-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2017 at 09:02 AM
-- Server version: 5.6.33-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `guesthouseonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `name` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`name`) VALUES
('Allengunj'),
('Allahpur'),
('Alopibagh'),
('Ashok Nagar'),
('Atala'),
('Attarsuiya'),
('Bahadurgunj'),
('Bai-Ka-Bagh'),
('Bairana'),
('Beli Colony'),
('Benigunj'),
('Bharadwaj Puram'),
('Cantonment - Allahabad Cantt'),
('Chatham Lines'),
('Chowk'),
('Civil Lines'),
('Colonelgunj'),
('Daragunj'),
('Darbhanga'),
('Dariyabad'),
('Dhoomangunj'),
('Georgetown'),
('Govindpur'),
('Johnstongunj'),
('Kareli'),
('Katra'),
('Khuldabad'),
('Khusrobagh'),
('Kydgunj'),
('Kalandipuram Colony'),
('Lukergunj'),
('Meerapur'),
('Meergunj'),
('Mumfordgunj'),
('Muirabad'),
('Mutthigunj'),
('North Malaka'),
('Pac Lines'),
('Police Lines'),
('Preetam Nagar'),
('Rajapur'),
('Rajrooppur'),
('Rambagh'),
('Rani Mandi'),
('Sadiyapur(Nishad Nagar)'),
('Satti Chaura'),
('Shastri Nagar'),
('Sohbatiabagh'),
('South Malaka'),
('Sulem Sarai'),
('Tagoretown'),
('Teliyargunj');

-- --------------------------------------------------------

--
-- Table structure for table `gardens`
--

CREATE TABLE IF NOT EXISTS `gardens` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `garden_number` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `length` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `price_per_hour` decimal(15,5) NOT NULL,
  PRIMARY KEY (`registrationNumber`,`garden_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gardens`
--

INSERT INTO `gardens` (`google_id`, `registrationNumber`, `garden_number`, `description`, `length`, `width`, `unit`, `price_per_hour`) VALUES
('111290723109336748051', '20170308092623', 'Garden1', 'A very beautiful and lush garden. The garden is also full of beautiful flowering plants.', 120, 100, 'metre', '99999.99999');

-- --------------------------------------------------------

--
-- Table structure for table `google_users`
--

CREATE TABLE IF NOT EXISTS `google_users` (
  `google_id` decimal(21,0) NOT NULL,
  `google_name` varchar(60) NOT NULL,
  `google_email` varchar(60) NOT NULL,
  `google_link` varchar(60) DEFAULT NULL,
  `google_picture_link` varchar(200) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `country_code` int(4) NOT NULL,
  PRIMARY KEY (`google_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `google_users`
--

INSERT INTO `google_users` (`google_id`, `google_name`, `google_email`, `google_link`, `google_picture_link`, `phone_number`, `country_code`) VALUES
('100139603335047609198', 'GradeUp Guru', 'shekharparallel@gmail.com', 'https://plus.google.com/100139603335047609198', 'https://lh4.googleusercontent.com/-7LKpAy7o76s/AAAAAAAAAAI/AAAAAAAAACg/CsZeTqO63HM/photo.jpg', '786484943', 91),
('100236411431087476835', 'ravi verma', 'ramvilaguesthouse@gmail.com', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '8601772460', 91),
('100749739706102842715', 'kshitij shukla', 'kshitij.shukla04@gmail.com', 'https://plus.google.com/100749739706102842715', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '0', 0),
('101344949920279432989', 'Subham Choudhury', 'iec2015076@iiita.ac.in', 'https://plus.google.com/101344949920279432989', 'https://lh5.googleusercontent.com/-J_HX0gzqpEw/AAAAAAAAAAI/AAAAAAAAAME/xi-aJy6YhOc/photo.jpg', '', 0),
('101582025576163847554', 'Nishant Shekhar', 'nishantshekhar1995@gmail.com', 'https://plus.google.com/101582025576163847554', 'https://lh3.googleusercontent.com/-dQKMjZI4zeE/AAAAAAAAAAI/AAAAAAAAAGU/z3YpAZdLR1c/photo.jpg', '7376335635', 91),
('103325011849113575224', 'Arvind Tripathi', 'arvindtripathi1978@gmail.com', 'https://plus.google.com/103325011849113575224', 'https://lh5.googleusercontent.com/-RgwU5zd1w-s/AAAAAAAAAAI/AAAAAAAAAac/keGwu9o_mFY/photo.jpg', '', 0),
('104579407647764370356', 'awanish shukla', 'awanishshukla1994@gmail.com', 'https://plus.google.com/104579407647764370356', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', 0),
('104988735532313714977', 'ABHAY VISHWAKARMA', 'abhaay1995@gmail.com', 'https://plus.google.com/104988735532313714977', 'https://lh4.googleusercontent.com/-X-ONFa3SQi4/AAAAAAAAAAI/AAAAAAAAABE/fuP6qFsOLVc/photo.jpg', '', 0),
('107630796566721516340', 'Rahul Ojha', 'rojha368@gmail.com', 'https://plus.google.com/107630796566721516340', 'https://lh3.googleusercontent.com/-A4VKcGveiQ0/AAAAAAAAAAI/AAAAAAAADBo/2lf9IJuCLvU/photo.jpg', '', 0),
('108243880738819301575', 'Sushila Yadav', 'sushilayadav.sy11@gmail.com', 'https://plus.google.com/108243880738819301575', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '0', 0),
('108983504331271024940', 'Ankit Tripathi', 'tripathiankit647@gmail.com', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', 0),
('109212364860129214898', 'nishant shekhar', 'nishantshekhar2013@gmail.com', 'https://plus.google.com/109212364860129214898', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', 0),
('111290723109336748051', 'Ayush Raj', 'ayushraj1024@gmail.com', 'https://plus.google.com/111290723109336748051', 'https://lh4.googleusercontent.com/-RnR8R1ib2rk/AAAAAAAAAAI/AAAAAAAAAD0/-nhN_MyVKjw/photo.jpg', '9954678943', 91),
('111921655611549351483', 'Ankit Tripathi', 'ankittripathi233@gmail.com', 'https://plus.google.com/111921655611549351483', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '0', 0),
('112133334674115575121', 'Raj Bali Yadav', 'rbaliyadav@gmail.com', 'https://plus.google.com/112133334674115575121', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '0', 91),
('112749930966347238728', 'prem chandra', 'premchandra1982@gmail.com', 'https://plus.google.com/112749930966347238728', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', 0),
('112817261498028520297', 'Prakhar Saran', 'prakhar049@gmail.com', 'https://plus.google.com/112817261498028520297', 'https://lh6.googleusercontent.com/-cl92m2QLgVM/AAAAAAAAAAI/AAAAAAAAAR8/Lo9P07mUJnY/photo.jpg', '', 0),
('115782273739236761902', 'ramapati gupta', 'hotelrppalace@gmail.com', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '9918041846', 91),
('116416876788885726411', 'Prashant Nayak', 'ibm2013014@iiita.ac.in', 'https://plus.google.com/+PrashantNayakCEO', 'https://lh4.googleusercontent.com/-nOm7ODn1N7U/AAAAAAAAAAI/AAAAAAAAABc/CQFeql71i-g/photo.jpg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `guesthouse`
--

CREATE TABLE IF NOT EXISTS `guesthouse` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `guestHouseName` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `address` varchar(250) NOT NULL,
  `pictures` varchar(50) NOT NULL,
  `bookingStatus` varchar(50) NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`registrationNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guesthouse`
--

INSERT INTO `guesthouse` (`google_id`, `registrationNumber`, `guestHouseName`, `area`, `address`, `pictures`, `bookingStatus`, `thumbnail`, `description`) VALUES
('101582025576163847554', 'Ab1224', 'my house', 'Allengunj', '34, stanley road', '', '', '1482735032.jpg', 'Situated in the heart of the city, The ABC Guest House, one of the best Budget guest house in ALLAHABAD will cater to all your requirements and preferences.\r\nWe have following facilities :\r\n\r\n1) Transportaion\r\n\r\n2) Food Catering for all occassions\r\n\r\n3) Rooms for guests to stay\r\n\r\n4) Halls for all occassions\r\n\r\n5) Gardens for all occassions\r\n\r\nFeel free to contact us for any further query.\r\nPh no: 999999999999'),
('115782273739236761902', '09312729400S', 'R.P. Palace', 'Police Lines', '23/1/83, Stanley Road, Allahabad\r\n(Opposite Police Line VIP Gate No.11)', '', '', '1484219427.jpg', ''),
('111290723109336748051', '20170308092623', 'Test house 1', 'Colonelgunj', 'fine streets of the area', '', '', '1488988583.jpg', 'A very cool guesthouse.'),
('100236411431087476835', '20170216094119', 'Ram Villa', 'Tagoretown', '16/1/16A ,Jawahar Lal Nehru Road, Tagore Town, Allahabad', '', '', '1487261480.jpg', 'Situated in the posh area of the city, our Guesthouse provides :\r\n1. Full power backup\r\n2. 24 hours security\r\n3. Wifi enabled area\r\n4 .Full lightning\r\n5. Full flower decoration\r\n6. Catering for 700 people\r\n7. Garden for dinner');

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE IF NOT EXISTS `halls` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `hall_number` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `length` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `price_per_hour` decimal(15,5) NOT NULL,
  PRIMARY KEY (`registrationNumber`,`hall_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halls`
--

INSERT INTO `halls` (`google_id`, `registrationNumber`, `hall_number`, `description`, `length`, `width`, `unit`, `price_per_hour`) VALUES
('101582025576163847554', 'Ab1224', 'hall1', 'all the action takes place here', 20, 20, 'metre', '10000.00000'),
('111290723109336748051', '20170308092623', 'Hall1', 'A very spacious hall. Perfect for parties.', 12, 14, 'metre', '12000.00000');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `memberID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `resetToken` varchar(255) DEFAULT NULL,
  `resetComplete` varchar(3) DEFAULT 'No',
  `profilePicture` blob,
  PRIMARY KEY (`memberID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberID`, `username`, `password`, `email`, `active`, `resetToken`, `resetComplete`, `profilePicture`) VALUES
(16, 'keyboardsan', '$2y$10$/bRFd9zcDw3LB9XoyxvT9.ZipiTYKxAjS1M6wQR7JotaenEuRCdFa', 'ayushraj1024@gmail.com', 'b5b9c9acd1f09b3f22b6ac6e671f72e1', NULL, 'No', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `package_number` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `no_of_halls` int(5) NOT NULL,
  `no_of_rooms` int(5) NOT NULL,
  `no_of_gardens` int(5) NOT NULL,
  `price_per_day` decimal(21,5) NOT NULL,
  `unit_of_measurement` varchar(10) NOT NULL,
  PRIMARY KEY (`google_id`,`registrationNumber`,`package_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`google_id`, `registrationNumber`, `package_number`, `description`, `no_of_halls`, `no_of_rooms`, `no_of_gardens`, `price_per_day`, `unit_of_measurement`) VALUES
('115782273739236761902', '09312729400S', '2', 'This package includes :\r\n1 hall for all occasions\r\n2 well-furnished rooms', 1, 2, 0, '40000.00000', 'feet'),
('115782273739236761902', '09312729400S', '1', 'This package includes :\r\n1 hall for all occasions\r\n2 well- furnished rooms', 1, 2, 0, '40000.00000', 'feet'),
('101582025576163847554', 'Ab1224', '1A', 'package a', 1, 4, 1, '150000.00000', 'metre'),
('101582025576163847554', 'Ab1224', 'Ab', '1 hall\r\n2 room', 1, 2, 1, '40000.00000', 'metre'),
('100236411431087476835', '20170216094119', '1', 'Package includes :\r\n1 hall for all occasions\r\n5 well furnished AC rooms\r\n5 well furnished non-AC rooms\r\nLawn area for dinner \r\nFull lightning\r\nFull flower decoration\r\n', 1, 10, 1, '150000.00000', 'feet'),
('111290723109336748051', '20170308092623', '1', 'First package', 1, 4, 1, '120000.00000', 'metre');

-- --------------------------------------------------------

--
-- Table structure for table `package_sections`
--

CREATE TABLE IF NOT EXISTS `package_sections` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `package_number` varchar(250) NOT NULL,
  `section_number` varchar(250) NOT NULL,
  `id` varchar(10) NOT NULL,
  `section_description` longtext NOT NULL,
  `length` int(10) NOT NULL,
  `width` int(10) NOT NULL,
  PRIMARY KEY (`google_id`,`registrationNumber`,`package_number`,`section_number`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_sections`
--

INSERT INTO `package_sections` (`google_id`, `registrationNumber`, `package_number`, `section_number`, `id`, `section_description`, `length`, `width`) VALUES
('115782273739236761902', '09312729400S', '1', '1', 'hall', 'A beautiful hall having capacity of 120 people at a time ; suitable for all occasions.', 1700, 1700),
('115782273739236761902', '09312729400S', '1', '2', 'room', 'Well furnished room having : \r\n1. Air-Conditioner\r\n2. Double-Bed\r\n3. T.V.\r\n4. Sofa set\r\n5. Clean Washroom\r\n6. Clean Bathroom', 50, 50),
('115782273739236761902', '09312729400S', '1', '1', 'room', 'Well furnished room having : \r\n1. Air-Conditioner\r\n2. Double-Bed\r\n3. T.V.\r\n4. Sofa set\r\n5. Clean Washroom\r\n6. Clean Bathroom', 50, 50),
('101582025576163847554', 'Ab1224', 'Ab', '1', 'garden', '', 0, 0),
('101582025576163847554', 'Ab1224', 'Ab', '1', 'hall', '', 0, 0),
('101582025576163847554', 'Ab1224', 'Ab', '2', 'room', '', 0, 0),
('101582025576163847554', 'Ab1224', 'Ab', '1', 'room', '', 0, 0),
('101582025576163847554', 'Ab1224', '1A', '1', 'garden', 'g', 100, 20),
('101582025576163847554', 'Ab1224', '1A', '4', 'room', 'a', 18, 10),
('101582025576163847554', 'Ab1224', '1A', '1', 'hall', 'dh', 10, 10),
('101582025576163847554', 'Ab1224', '1A', '3', 'room', 'a', 18, 10),
('101582025576163847554', 'Ab1224', '1A', '2', 'room', 'a', 18, 10),
('101582025576163847554', 'Ab1224', '1A', '1', 'room', 'a', 18, 10),
('115782273739236761902', '09312729400S', '2', '1', 'room', '', 0, 0),
('115782273739236761902', '09312729400S', '2', '2', 'room', '', 0, 0),
('115782273739236761902', '09312729400S', '2', '1', 'hall', '', 0, 0),
('100236411431087476835', '20170216094119', '1', '1', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '2', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '3', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '4', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '5', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '6', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '7', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '8', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '9', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '10', 'room', 'Well furnished non AC room', 20, 10),
('100236411431087476835', '20170216094119', '1', '1', 'hall', '1.Spacious hall for all occasions \r\n2. Full flower decoration', 40, 40),
('100236411431087476835', '20170216094119', '1', '1', 'garden', 'Spacious garden for Dinner', 60, 80),
('111290723109336748051', '20170308092623', '1', '1', 'room', '', 13, 8),
('111290723109336748051', '20170308092623', '1', '2', 'room', '', 7, 5),
('111290723109336748051', '20170308092623', '1', '3', 'room', '', 8, 7),
('111290723109336748051', '20170308092623', '1', '4', 'room', '', 7, 10),
('111290723109336748051', '20170308092623', '1', '1', 'hall', '', 100, 67),
('111290723109336748051', '20170308092623', '1', '1', 'garden', '', 100, 50);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `id` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`google_id`, `registrationNumber`, `id`, `name`) VALUES
('111290723109336748051', '20170308092623', 'guest_house', '14890326142.jpg'),
('111290723109336748051', '20170308092623', 'guest_house', '14890326141.jpg'),
('111290723109336748051', '20170308092623', 'guest_house', '14890326140.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626196.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626195.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626194.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626193.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827349820.jpg'),
('101582025576163847554', 'Ab1224', 'wedding hall', '14831453500.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626192.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626191.jpg'),
('101582025576163847554', 'Ab1224', '2', '14827377620.jpg'),
('100236411431087476835', '20170216094119', '1', '14872626190.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827384381.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619029.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619028.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619027.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827419932.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827419933.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827419934.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827419935.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827420446.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827420447.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827420448.jpg'),
('101582025576163847554', 'Ab1224', 'guest_house', '14827420449.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619026.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619025.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619024.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619023.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619022.jpg'),
('101582025576163847554', 'Ab1224', '', '14831451660.jpg'),
('101582025576163847554', 'Ab1224', '', '14831453701.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619021.jpg'),
('100236411431087476835', '20170216094119', 'guest_house', '14872619020.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842775039.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842775038.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771467.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771466.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771465.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771464.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771463.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771462.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771461.jpg'),
('111290723109336748051', '20170112100405', 'guest_house', '14842389733.jpg'),
('111290723109336748051', '20170112100405', 'guest_house', '14842389732.jpg'),
('111290723109336748051', '20170112100405', 'guest_house', '14842389731.jpg'),
('111290723109336748051', '20170112100405', 'guest_house', '14842389730.jpg'),
('111290723109336748051', '20170110124333', 'guest_house', '14842373330.jpg'),
('111290723109336748051', '20170110124333', 'guest_house', '14842373331.jpg'),
('111290723109336748051', '20170110124333', 'guest_house', '14842373332.jpg'),
('111290723109336748051', '20170110124333', 'guest_house', '14842373333.jpg'),
('115782273739236761902', '09312729400S', 'guest_house', '14842771460.jpg'),
('111290723109336748051', '20170110124333', 'guest_house', '14842373334.jpg'),
('111290723109336748051', '20170308092623', 'guest_house', '14890326143.jpg'),
('111290723109336748051', '20170308092623', 'guest_house', '14890326144.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630930.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630931.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630932.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630933.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630934.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630935.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630936.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630937.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630938.jpg'),
('111290723109336748051', '20170308092623', '1', '14890630939.jpg'),
('111290723109336748051', '20170308092623', 'Hall1', '14890695840.jpg'),
('111290723109336748051', '20170308092623', 'Hall1', '14890695841.jpg'),
('111290723109336748051', '20170308092623', 'Hall1', '14890695842.jpg'),
('111290723109336748051', '20170308092623', 'Hall1', '14890694907.jpg'),
('111290723109336748051', '20170308092623', 'Hall1', '14890695847.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890698460.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890698461.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890697544.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890697548.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890698464.jpg'),
('111290723109336748051', '20170308092623', 'Room1', '14890698468.jpg'),
('111290723109336748051', '20170308092623', 'Garden1', '14890700280.jpg'),
('111290723109336748051', '20170308092623', 'Garden1', '14890700281.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `google_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `room_number` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `length` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `price_per_hour` decimal(15,5) NOT NULL,
  PRIMARY KEY (`registrationNumber`,`room_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`google_id`, `registrationNumber`, `room_number`, `description`, `length`, `width`, `unit`, `price_per_hour`) VALUES
('101582025576163847554', 'Ab1224', '1', 'room for the bride', 20, 20, 'feet', '1500.00000'),
('101582025576163847554', 'Ab1224', '2', 'a.c.room', 15, 15, 'feet', '1000.00000'),
('111290723109336748051', '20170308092623', 'Room1', 'A room with comfortable bed and chairs. Cupboard and desk also included.', 1, 3, 'metre', '120.00000');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `customer_id` decimal(21,0) NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `place_number` varchar(250) NOT NULL,
  `time_from` varchar(250) NOT NULL,
  `time_to` varchar(250) NOT NULL,
  `booking_time` varchar(250) NOT NULL,
  `amount` decimal(20,4) NOT NULL,
  `amount_paid` decimal(20,4) NOT NULL,
  `confirmation` varchar(250) NOT NULL,
  `completion` varchar(250) NOT NULL,
  PRIMARY KEY (`booking_time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`customer_id`, `registrationNumber`, `place_number`, `time_from`, `time_to`, `booking_time`, `amount`, `amount_paid`, `confirmation`, `completion`) VALUES
('112133334674115575121', 'Ab1224', '1', '01/01/2017 4:49 PM', '01/02/2017 4:50 PM', '20161231045012pm', '16500.0000', '0.0000', 'pending', 'cancelled'),
('100749739706102842715', 'Ab1224', '2', '12/29/2016 5:38 PM', '12/30/2016 5:38 PM', '20161227053836pm', '166.6667', '0.0000', 'pending', 'pending'),
('109212364860129214898', 'Ab1224', '1', '12/31/2016 2:18 PM', '01/11/2017 2:18 PM', '20161231021810pm', '165000.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', '20170308092623', '1', '03/11/2017 10:59 AM', '03/12/2017 10:59 AM', '20170309105930am', '50000.0000', '0.0000', 'confirmed', 'pending'),
('100749739706102842715', 'Ab1224', '1', '12/28/2016 5:30 PM', '12/29/2016 5:30 PM', '20161227053128pm', '250.0000', '250.0000', 'confirm', 'confirm'),
('111290723109336748051', '20170308092623', 'Garden1', '03/09/2017 10:50 AM', '03/10/2017 10:50 AM', '20170309105058am', '0.0000', '0.0000', 'confirmed', 'pending'),
('100139603335047609198', 'Ab1224', '1', '12/29/2016 8:30 PM', '12/30/2016 8:30 PM', '20161227083025pm', '250.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', '20170308092623', 'Room1', '03/09/2017 10:49 AM', '03/10/2017 10:49 AM', '20170309104936am', '0.0000', '0.0000', 'confirmed', 'pending'),
('111290723109336748051', '20170308092623', 'Hall1', '03/09/2017 10:48 AM', '03/10/2017 10:48 AM', '20170309104850am', '120000.0000', '0.0000', 'confirmed', 'pending'),
('111290723109336748051', '20170308092623', '1', '03/09/2017 10:45 AM', '03/10/2017 10:45 AM', '20170309104547am', '0.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', 'Ab1224', 'hall1', '03/21/2017 10:41 AM', '03/22/2017 10:41 AM', '20170308104152am', '100000.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', 'Ab1224', 'hall1', '03/23/2017 10:37 AM', '03/24/2017 10:37 AM', '20170308103749am', '100000.0000', '0.0000', 'pending', 'pending'),
('108983504331271024940', '09312729400S', '1', '01/21/2017 6:00 PM', '01/21/2017 9:00 PM', '20170121035533pm', '0.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', 'Ab1224', '1', '12/04/2016 6:31 PM', '12/06/2016 6:31 PM', '20161231063131pm', '30000.0000', '0.0000', 'pending', 'pending'),
('109212364860129214898', 'Ab1224', '2', '01/03/2017 2:32 PM', '01/05/2017 2:32 PM', '20170103023250pm', '20000.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', '09312729400S', '2', '02/28/2017 9:56 PM', '03/01/2017 9:56 PM', '20170204095703pm', '440000.0000', '0.0000', 'pending', 'cancelled'),
('111290723109336748051', 'Ab1224', '1', '02/23/2017 3:25 PM', '02/28/2017 3:25 PM', '20170204032507pm', '75000.0000', '0.0000', 'pending', 'pending'),
('111290723109336748051', 'Ab1224', '1', '02/12/2017 12:40 PM', '02/13/2017 12:40 PM', '20161230124038pm', '10000.0000', '1000.0000', 'confirmed', 'completed'),
('109212364860129214898', 'Ab1224', 'wedding hall', '12/31/2016 6:28 AM', '01/01/2017 6:28 AM', '20161231062836am', '50000.0000', '0.0000', 'pending', 'pending'),
('108243880738819301575', 'Ab1224', 'hall1', '02/23/2017 2:27 PM', '02/24/2017 2:27 PM', '20170108022744pm', '100000.0000', '0.0000', 'pending', 'pending');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
