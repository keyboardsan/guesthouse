<?php
session_start(); //session start
require_once('includes/config.php');
require_once ('libraries/Google/autoload.php');

//Insert your client ID and secret 
//You can get it from : https://console.developers.google.com/
$client_id = '635568245346-fqj3eo25ckn41jb6raip7duv4usggrgq.apps.googleusercontent.com'; 
$client_secret = '4TOjkJCCyh0L7IWi-CGlE5n9';
$redirect_uri = 'http://guesthouseonline.co.in/login.php';

//database


//incase of logout request, just unset the session var
if (isset($_GET['logout'])) {
  unset($_SESSION['access_token']);
  unset($_SESSION['name']);
  unset($_SESSION['id']);
  unset($_SESSION['profile_picture_url']);
}

/************************************************
  Make an API request on behalf of a user. In
  this case we need to have a valid OAuth 2.0
  token for the user, so we need to send them
  through a login flow. To do this we need some
  information from our API console project.
 ************************************************/
$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->addScope("email");
$client->addScope("profile");

/************************************************
  When we create the service here, we pass the
  client to it. The client then queries the service
  for the required scopes, and uses that when
  generating the authentication URL later.
 ************************************************/
$service = new Google_Service_Oauth2($client);

/************************************************
  If we have a code back from the OAuth 2.0 flow,
  we need to exchange that with the authenticate()
  function. We store the resultant access token
  bundle in the session, and redirect to ourself.
*/
  
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  exit;
}

/************************************************
  If we have an access token, we can make
  requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  if($client->isAccessTokenExpired()) {

    $authUrl = $client->createAuthUrl();
    header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));

  }
} else {
  $authUrl = $client->createAuthUrl();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <script>
  function closewindow() {
  	window.close();
  	}
  </script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<!--<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>-->
  
<div class="col-xs-12 text-center">    
  <center>
	<div style = "padding-top:10%;margin-left:2%;">
	<h1>Guest House Online</h1>
	</div>
	<div class="col-xs-12">
<?php		
if (isset($authUrl)){ 
	//show login url
	echo '<div align="center">';
        echo '<br><br>';
	echo '<a class="login" href="' . $authUrl . '"><img src="images/google_login.png" width = "200" height = "auto" /></a>';
	echo '</div>';
	
} else {
	
	$user = $service->userinfo->get(); //get user info 
	
	// connect to database
	$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
	
	//check if user exist in database using COUNT
	$result = $mysqli->query("SELECT COUNT(google_id) as usercount FROM google_users WHERE google_id=$user->id");
	$user_count = $result->fetch_object()->usercount; //will return 0 if user doesn't exist
	
	//show user picture
        echo '<br>';
	echo '<img src="'.$user->picture.'" width = "50" height = "auto" style = "border-radius:50%;" />';
        echo '<br><br>';
	if($user_count>0) //if user already exist change greeting text to "Welcome Back"
    {
        $_SESSION['name'] = $user->name;
        $_SESSION['id'] = $user->id;
        $_SESSION['profile_picture_url'] = $user->picture;
        $_SESSION['google_plus_profile'] = $user->link;
        echo 'Welcome back '.$user->name.'<br> <a href = "#" onclick = window.close()>Close this window</a> <br>or <br> <a href = "index.php">Enter Site</a><br><a href="index.php?logout=1">Log Out</a>';
    }
	else //else greeting text "Thanks for registering"
	{ 
	$_SESSION['name'] = $user->name;
        $_SESSION['id'] = $user->id;
        $_SESSION['profile_picture_url'] = $user->picture;
        $_SESSION['google_plus_profile'] = $user->link;
        echo 'Hi '.$user->name.', Thanks for Registering! <br> <a href = "index.php">Enter Site</a> <br> <a href="index.php?logout=1">Log Out</a>';
		$statement = $mysqli->prepare("INSERT INTO google_users (google_id, google_name, google_email, google_link, google_picture_link) VALUES (?,?,?,?,?)");
		$statement->bind_param('sssss', $user->id,  $user->name, $user->email, $user->link, $user->picture);
		$statement->execute();
		echo $mysqli->error;
    }
}
echo '</div>';



?>
	</div>
	
	<!--<div class="col-sm-2 sidenav">
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
    </div>-->
    <!--<div class="col-sm-8 text-left"> 
      <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>
      <h3>Test</h3>
      <p>Lorem ipsum...</p>
    </div>-->
    <!--<div class="col-sm-2 sidenav">
      <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div>
    </div>-->
    </center>
</div>

<!--<footer class="container-fluid text-center">
  <p>Website developed by: keyboardsan</p>
</footer>-->

</body>
</html>
