<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
  
}
unset($_SESSION["bookingErrorMessage"]);
date_default_timezone_set('Asia/Kolkata');

$target_dir = "uploads/";
unset($_SESSION["slideshowErrorMessage"]);
unset($_SESSION["errorMessage"]);
 $time_from = new DateTime($_POST['time_from']);
  $time_to = new DateTime($_POST['time_to']);
   $difference = $time_from->diff($time_to);
  if($difference->format('%a%d')<1) {
  	$_SESSION["bookingErrorMessage"] = "You have to give the time for atleast 1 day";
  	header('Location: browse.php');
  	exit();
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <script src="moment-with-locales.js"></script>
  <script src="bootstrap-datetimepicker.js"></script>
  <link href="bootstrap-datetimepicker.css" rel="stylesheet">
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>
  <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker();
            });
        </script>
  
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center" style = "width:300px;">    
  <div class="row">
<br><br><br>
<h4>The guest house packages currently free at the specified time slot are:</h4>
<?php 
  $i = 0;
  $package_number = array();
  $registrationNumber = array();
   $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT package_number,registrationNumber FROM packages";
    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  		$package_number[$i] = $row["package_number"];
  		$registrationNumber[$i] = $row["registrationNumber"];
  		$i++;
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
  $slot_free_number = array();
  $j = 0;
  $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    for($j=0;$j<$i;$j++) {
    $slot_free_number[$j] = -1;
    $sql="SELECT time_from,time_to FROM status WHERE place_number = '".$package_number[$j]."' and registrationNumber = '".$registrationNumber[$j]."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  		$time_from_status = new DateTime($row["time_from"]);
  		$time_to_status = new DateTime($row["time_to"]);
  		
  		if($time_from == $time_from_status) {
  			$slot_free_number[$j] = 0;
  			continue;
  		}
  		if($time_from == $time_to_status) {
  			$slot_free_number[$j] = 0;
  			continue;
  		}
  		if($time_to > $time_from_status && $time_to < $time_to_status) {
  			$slot_free_number[$j] = 0;
  			continue;
  		}
  		if($time_from > $time_from_status && $time_from < $time_to_status) {
  			$slot_free_number[$j] = 0;
  			continue;
  		}
  		
  	}
  mysqli_free_result($result);
  }
  if($slot_free_number[$j]==(-1)) {
  $slot_free_number[$j] = 1;
  }
}
  $k=0;
  for($k=0;$k<$i;$k++) {
  	if($slot_free_number[$k]>0) {
  		$sql="SELECT thumbnail,guestHouseName,area,registrationNumber FROM guesthouse WHERE registrationNumber = '".$registrationNumber[$k]."'";
  		if ($result=mysqli_query($mysqli,$sql))
  		{
  			while ($row=mysqli_fetch_assoc($result))
  			{
  		 	echo '<h4><a href = "view_package.php?registrationNumber='.$row["registrationNumber"].'&package_number='.$package_number[$k].'" class = "btn btn-default"><img src = "'.$target_dir.$row["thumbnail"].'" width = "100" height = "auto">&emsp;'.$row["guestHouseName"].', '.$row["area"].'<br>Package Number: '.$package_number[$k].'<br>registration number:'.$row["registrationNumber"].'</a><br><br>';
  	}
  	mysqli_free_result($result);
  	}
  	}
  	else if($slot_free_number[$k]<=0) {
  		continue;
  	}
  }
mysqli_close($mysqli);
?>
</div>
</div>
</body>
</html>