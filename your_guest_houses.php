<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}
$target_dir = "uploads/";



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center visible-md visible-lg hidden-sm hidden-xs">    

  <br><br><br>
  <h4>Manage your guest houses here</h4>
  <h5>Click on a guest house to manage it</h5>
  <br><br>
  <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT guestHouseName,registrationNumber,area,thumbnail FROM guesthouse where google_id = '".$_SESSION['id']."'";
    
    if ($result=mysqli_query($mysqli,$sql))
  {
  	if(mysqli_num_rows($result)<=0) {
  	 echo '<br><br><h4>You have not hosted any guest houses</h4><a href = "host_guest_house.php" class = "btn btn-primary">Host a new guest house</a>';
  	 } else {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<a href = "manage_guest_house.php?registrationNumber='.$row["registrationNumber"].'" class = "btn btn-default"><img src = "'.$target_dir.$row["thumbnail"].'" width = "100" height = "auto">&emsp;'.$row["guestHouseName"].', '.$row["area"].'<br><br>registration number:'.$row["registrationNumber"].'</a><br><br>';
    }
  // Free result set
  mysqli_free_result($result);
}
}


mysqli_close($mysqli);
?>

</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs">    
 
  <br><br><br>
  <h4>Manage your guest houses here</h4>
  <h5>Click on a guest house to manage it</h5>
  <br><br>
  <?php
       $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT guestHouseName,registrationNumber,area,thumbnail FROM guesthouse where google_id = '".$_SESSION['id']."'";
    
    if ($result=mysqli_query($mysqli,$sql))
  {
  	if(mysqli_num_rows($result)<=0) {
  	 echo '<br><br><h4>You have not hosted any guest houses</h4><a href = "host_guest_house.php" class = "btn btn-primary">Host a new guest house</a>';
  	 } else {
  // Fetch one and one row
  while ($row=mysqli_fetch_assoc($result))
    {
    echo '<a href = "manage_guest_house.php?registrationNumber='.$row["registrationNumber"].'" class = "btn btn-default"><img src = "'.$target_dir.$row["thumbnail"].'" width = "100" height = "auto">&emsp;'.$row["guestHouseName"].', '.$row["area"].'<br><br>registration number:'.$row["registrationNumber"].'</a><br><br>';
    }
  // Free result set
  mysqli_free_result($result);
}
}


mysqli_close($mysqli);
?>
</div>
</body>
</html>