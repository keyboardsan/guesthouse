<?php
session_start();
require_once('includes/config.php');
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
$valid_session = 0;
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  	}
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: http://guesthouseonline.co.in');
  	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <script>
  function uploading() {
  document.getElementById("uploader").innerHTML = "Uploading Files. Please wait.";
  }
  </script>
  <script>
  function readURL(input,image) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'.concat(image)).attr('src', e.target.result);
        }
        

        reader.readAsDataURL(input.files[0]);
    }
}
   function show_default_image(input) {
	input.src = "images/image_not_found.jpg";
}
  </script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 40%;
      margin: auto;
  }
  .carousel-inner {
  background-color: lightgrey;
  }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>
<?php
	$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT description,price_per_hour FROM rooms where google_id = '".$_SESSION['id']."' and registrationNumber = '".$_GET['registrationNumber']."' and room_number = '".$_GET['room_number']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
           $price_per_hour = $row["price_per_hour"];
           $description = $row["description"];
  	}
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
?>
  
<div class="col-xs-12 text-center">    
  <center> 
  <br><br><br>
  <form method = "post" enctype = "multipart/form-data" action = "upload_room_profile.php?registrationNumber=<?php echo $_GET["registrationNumber"];?>&room_number=<?php echo $_GET['room_number']; ?>" style = "max-width:300px;">
  <h4>Edit description</h4>
  <textarea name = "description" class = "form-control" rows = "10" cols = "5"><?php echo $description; ?></textarea>
  <br>
  <h4>Upload images for slideshow</h4>
  <p class="bg-danger"><?php echo $_SESSION["slideshowErrorMessage"]; ?></p><br>
  <?php
  $i = 0;
  $images = array();
  $num = 0;
  $mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
  $default_image = "images/image_not_found.jpg";
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT name FROM pictures where google_id = '".$_SESSION['id']."' and registrationNumber = '".$_GET['registrationNumber']."' and id = '".$_GET['room_number']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
           $images[$i] = $row["name"];
           $i++;
        }
  $num = $i;
  mysqli_free_result($result);
  }
mysqli_close($mysqli);
for($i = 0; $i<10; $i++) {
           $image_name = "image_".$i;
           echo ($i+1).')&emsp;<label class="btn btn-primary" for="my-file-selector_'.$i.'">
  <input id="my-file-selector_'.$i.'"  onchange="readURL(this,&quot;'.$image_name.'&quot;)" type="file" name = "fileToUpload_'.$i.'" style="display:none;">
    Browse
  </label>
  <img id="image_'.$i.'" src="'.$target_dir.$images[$i].'" width = "100" height = "auto" onerror="show_default_image(this)"/>
  <br><br>';
}
?>
<input type = "submit" class = "btn btn-success" value  = "submit" onclick = "uploading()">
</form>
<br>
<p id = "uploader"><b></b></p>
<br><br>
</center>
</div>
</body>
</html>