<?php
require_once('includes/config.php');
session_start();
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}

$target_dir = "uploads/";
unset($_SESSION["slideshowErrorMessage"]);
$valid_session = 0;
$mysqli = new mysqli($host_name, $db_username, $db_password, $db_name);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $sql="SELECT registrationNumber FROM guesthouse where google_id = '".$_SESSION['id']."'";

    if ($result=mysqli_query($mysqli,$sql))
  {
  	while ($row=mysqli_fetch_assoc($result))
  	{
  	if($row["registrationNumber"]==$_GET["registrationNumber"]) {
  		$valid_session = 1;
  		break;
  	} else {
  		$valid_session = 0;
  	}
  }
  mysqli_free_result($result);
  
}
mysqli_close($mysqli);
if($valid_session==0) {
	header('Location: index.php');
  	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      object-fit:cover;
  }
  .carousel {
  	height:400px;
  }
  
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="col-xs-6 text-center visible-md visible-lg hidden-sm hidden-xs">    
  <center>
  <br><br><br>
  <form action = "upload_garden_data.php?registrationNumber=<?php echo $_GET['registrationNumber']; ?>" method = "post" enctype="multipart/form-data" style="width:300px;">  
  <p class="bg-danger"><?php echo $_SESSION['errorMessage']; ?></p><br>
  <h4>&emsp; &emsp; Enter garden number &emsp;</h4>
  <h4>(garden number should be unique for your guest house)</h4>
  <input type = "text" name = "garden_number" id = "garden_number" placeholder = "garden number (numbers or alphabets or both)" class = "form-control input-sm" required><br><br> 
  <h4>Garden description</h4>
  <textarea name = "description" class = "form-control" rows = "10" cols = "5" required></textarea><br><br>
  <h4>Garden size</h4><h4>(enter length and width of floor)</h4>
  <input type = "number" name = "length" id = "length" placeholder = "length" class = "form-control input-sm" required>
  &emsp;X&emsp;
  <input type = "number" name = "width" id = "width" placeholder = "width" class = "form-control input-sm" required>
  <br><br>
  <h4>Select unit of floor measurements</h4>
  <select name="unit" id = "unit" class = "form-control">
  <option value="metre">metre</option>
  <option value="inch">inch</option>
  <option value="feet">feet</option>
  <option value="cm">cm</option>
  </select><br>
  <h4>Enter price per hour</h4>
  <input type = "number" name = "price" id = "price" value = "price of booking the garden per hour" class = "form-control" required><br>
  <input type = "submit" id = "submit" value = "submit" class = "btn btn-success">
  <br><br>
</form>
</center>
</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs">    
  <center>
  <br><br><br>
  <form action = "upload_garden_data.php?registrationNumber=<?php echo $_GET['registrationNumber']; ?>" method = "post" enctype="multipart/form-data" style="width:200px;">  
  <p class="bg-danger"><?php echo $_SESSION['errorMessage']; ?></p><br>
  <h4>Enter garden number &emsp;</h4>
  <h4>(garden number should be unique for your guest house)</h4>
  <input type = "text" name = "garden_number" id = "garden_number" placeholder = "garden number (numbers or alphabets or both)" class = "form-control input-sm" required><br><br> 
  <h4>Garden description</h4>
  <textarea name = "description" class = "form-control" rows = "10" cols = "5" required></textarea><br><br>
  <h4>Garden size</h4><h4>(enter length and width of floor)</h4>
  <input type = "number" name = "length" id = "length" placeholder = "length" class = "form-control input-sm" required>
  &emsp;X&emsp;
  <input type = "number" name = "width" id = "width" placeholder = "width" class = "form-control input-sm" required>
  <br><br>
  <h4>Select unit of floor measurements</h4>
  <select name="unit" id = "unit" class = "form-control">
  <option value="metre">metre</option>
  <option value="inch">inch</option>
  <option value="feet">feet</option>
  <option value="cm">cm</option>
  </select><br>
  <h4>Enter price per hour</h4>
  <input type = "number" name = "price" id = "price" value = "price of booking the garden per hour" class = "form-control" required><br>
  <input type = "submit" id = "submit" value = "submit" class = "btn btn-success">
  <br><br>
</form>
</center>
</div>
</body>
</html>