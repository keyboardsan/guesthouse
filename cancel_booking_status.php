<?php
session_start();
require_once('includes/config.php');
unset($_SESSION['errorMessage']);
if(!(isset($_SESSION['name']))) {
   header('Location: '.'login.php');
}


try {
    $conn = new PDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE status SET completion = 'cancelled' WHERE booking_time = '".$_GET['booking_time']."'";

    // Prepare statement
    $stmt = $conn->prepare($sql);

    // execute the query
    $stmt->execute();
    header('Location: manage_booking.php');
    }
catch(PDOException $e)
    {
    echo '<br>'.$e->getMessage();
    }
$conn = null;
?>