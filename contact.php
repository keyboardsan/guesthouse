<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guest House Online</title>
  <link rel='shortcut icon' href='images/favicon.png' type='image/x-icon' />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "stylesheet" href = "style/my_style.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-fixed-top" style = "background-color:white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style = "background-color:lightgrey; border-style:solid;border-bottom:solid grey;" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>
        <span class="icon-bar text-primary"></span>                         
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="browse_by_area.php">Guest Houses</a></li>
        <li><a href="about_us.php">About us</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION['id'])) {
       echo '
        <li><a href = "dashboard.php"><img src = "'.$_SESSION["profile_picture_url"].'" width = "20" height = "auto" style = "border-radius:50%;"></img>&emsp; Dashboard</a></li>
 <li><a href ="index.php?logout=1">Logout</a></li>';
 }
 else {
 	echo '<li><a href ="login.php">Login</a></li>';
 	}
 ?>
      </ul>
    </div>
  </div>
</nav>

  
<div class="container-fluid text-center visible-md visible-lg hidden-sm hidden-xs">    

  <br><br><br>
  <h3>Guest House Online</h3><br>
  <h5>If you are having trouble with anything, let us know</h5>
  <h5>Email us at:</h5><h5> support@guesthouseonline.co.in</h5>
  <br><br>
  <h5>Or, contact these people through their google plus profiles</h5><br>
  <h4>Administrators</h4><br>
  <a href = "https://plus.google.com/101582025576163847554" target = "_blank"><img src = "https://lh3.googleusercontent.com/-dQKMjZI4zeE/AAAAAAAAAAI/AAAAAAAAAGU/z3YpAZdLR1c/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Nishant Shekhar"></img></a>&emsp;&emsp;
  <a href = "https://plus.google.com/100749739706102842715" target = "_blank"><img src = "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Kshitij Shukla"></img></a>
  <br><br><br>
  <h4>Developers</h4><br>
  <a href = "https://plus.google.com/111290723109336748051" target = "_blank"><img src = "https://lh4.googleusercontent.com/-RnR8R1ib2rk/AAAAAAAAAAI/AAAAAAAAAD0/-nhN_MyVKjw/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Ayush Raj"></img></a>
  <br><br>

</div>

<div class="col-xs-12 text-center hidden-md hidden-lg visible-sm visible-xs">    

  <br><br><br>
  <h3>Guest House Online</h3><br>
  <h5>If you are having trouble with anything, let us know</h5>
  <h5>Email us at:</h5><h5> support@guesthouseonline.co.in</h5>
  <br><br>
  <h5>Or, contact these people through their google plus profiles</h5><br>
  <h4>Administrators</h4><br>
  <a href = "https://plus.google.com/101582025576163847554" target = "_blank"><img src = "https://lh3.googleusercontent.com/-dQKMjZI4zeE/AAAAAAAAAAI/AAAAAAAAAGU/z3YpAZdLR1c/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Nishant Shekhar"></img></a>&emsp;&emsp;
  <a href = "https://plus.google.com/100749739706102842715" target = "_blank"><img src = "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Kshitij Shukla"></img></a>
  <br><br><br>
  <h4>Developers</h4><br>
  <a href = "https://plus.google.com/111290723109336748051" target = "_blank"><img src = "https://lh4.googleusercontent.com/-RnR8R1ib2rk/AAAAAAAAAAI/AAAAAAAAAD0/-nhN_MyVKjw/photo.jpg" height = "50" width = "auto" style = "border-radius:50px;" title = "Ayush Raj"></img></a>
  <br><br>

</div>
</body>
</html>
  
  
  